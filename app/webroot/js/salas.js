
var Sistema = {
   Salas: {
      CarregarModalNovo: function(){
         $('#nome, #numero, #id').val('');
         $('#tipoModal').html('Novo');
         $('#modalEditar').modal('show');
      },
      
      CarregarModalEditar: function(idSala) {

         $('#nome, #numero, #id').val('');

         $.ajax({
            type: 'GET',
            url: 'Salas/carregarDados/' + idSala,
            data: '',
            dataType: 'json'

         }).done(function(dados) {
            
            console.log(dados);

            $('#modalEditar').modal('show');

            $('#id').val(dados.id);
            $('#nome').val(dados.nome);
            $('#numero').val(dados.numero);
            $('#tipoModal').html('Edição');

         }).fail(function(jqXHR, textStatus) {
            alert('Erro: ' + textStatus + '\n' + jqXHR);
            /*
             jqXHR.each(function(index,valor){
             alert('Chave: '+index+'==='+'Valor: '+valor);
             
             });
             */
         });
      },
      
      Editar: function() {
         
         var data = $('#form-salas').serialize();
         
         $.ajax({
            type: 'POST',
            url: 'Salas/Editar/',
            data: data,
            dataType: 'text'

         }).done(function(dados) {
            
            alert('Sala Salva com sucesso!!');
            
            //document.location = window.location.href;
            //window.location.reload();
            location.reload(true);
            

         }).fail(function(jqXHR, textStatus) {
            alert('Erro: ' + textStatus + '\n' + jqXHR);

         });         
      },      
      
      Ativar: function(obj) {
         var idSala = $(obj).attr('id');
         var check = $(obj).is(':checked') == true ? 1 : 0;

         $.ajax({
            type: 'GET',
            url: 'Salas/Ativar/' + idSala + '/' + check,
            data: '',
            dataType: 'text'

         }).done(function(dados) {

            var msg = check == 1 ? 'ATIVADA' : "DESATIVADA";

            alert('Sala ' + msg + ' com sucesso!');

         }).fail(function(jqXHR, textStatus) {
            alert('Erro: ' + textStatus + '\n' + jqXHR);

         });
      },
      Excluir: function(idSala) {

         if (confirm('Deseja excluir a Sala?')) {

            $.ajax({
               type: 'GET',
               url: 'Salas/Excluir/' + idSala,
               data: '',
               dataType: 'text'

            }).done(function(dados) {
               
               //return alert(dados);
               
               $('#tr_'+idSala).hide();
               
               alert('Sala excluída!');
               
               //document.location = window.location.href;
               //location.reload(true);
               window.location.reload();

            }).fail(function(jqXHR, textStatus) {
               alert('Erro: ' + textStatus + '\n' + jqXHR);

            });
         }
      }
   }
};