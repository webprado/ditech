
var Sistema = {
   Funcionarios: {
      CarregarModalNovo: function(){
         $('#id, #nome').val('');
         $('#tipoModal').html('Novo');
         $('#modalEditar').modal('show');
      },
      
      CarregarModalEditar: function(idFunc) {
         
         $('#tipoModal').html('Edição');
         $('input').val('');
         
         $.ajax({
            type: 'GET',
            url: 'Funcionarios/carregarDados/' + idFunc,
            data: '',
            dataType: 'json'

         }).done(function(dados) {
            
            console.log(dados);
            
            $.each(dados,function(campo,valor){
               $('#'+campo).val(valor);               
            });
                      
            $('#modalEditar').modal('show');

         }).fail(function(jqXHR, textStatus) {                       
            alert('Erro: ' + textStatus + '\n' + jqXHR);
        
         });
      },
      
      Editar: function() {
         
         var data = $('#form-funcionarios').serialize();
         
         $.ajax({
            type: 'POST',
            url: 'Funcionarios/Editar/',
            data: data,
            dataType: 'text'

         }).done(function(dados) {
            alert('Funcionário Salva com sucesso!!');
            
            //document.location = window.location.href;
            location.reload(true);

         }).fail(function(jqXHR, textStatus) {
            alert('Erro: ' + textStatus + '\n' + jqXHR);

         });         
      },      
      
      Ativar: function(obj) {
         var idFunc = $(obj).attr('id');
         var check = $(obj).is(':checked') == true ? 1 : 0;

         $.ajax({
            type: 'GET',
            url: 'Funcionarios/Ativar/' + idFunc + '/' + check,
            data: '',
            dataType: 'text'

         }).done(function(dados) {

            var msg = check == 1 ? 'ATIVADA' : "DESATIVADA";

            alert('Funcionário ' + msg + ' com sucesso!');

         }).fail(function(jqXHR, textStatus) {
            alert('Erro: ' + textStatus + '\n' + jqXHR);

         });
      },
      Excluir: function(idFunc) {

         if (confirm('Deseja excluir Funcionário?')) {

            $.ajax({
               type: 'GET',
               url: 'Funcionarios/Excluir/' + idFunc,
               data: '',
               dataType: 'text'
            }).done(function(dados) {               
               $('#tr_'+idFunc).hide();              
               location.reload(true);

            }).fail(function(jqXHR, textStatus) {
               alert('Erro: ' + textStatus + '\n' + jqXHR);

            });
         }
      }
   }
};