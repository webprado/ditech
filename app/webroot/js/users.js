
var Sistemas = {
   Usuarios: {
      CarregarModalEditarUsuario: function (idFunc, idUser, nomeFuncionario) {

      //console.log('Carregando...');

         var URL = '';

         if (idUser != 0) {
            URL = 'Users/carregarDados/' + idFunc + '/' + idUser;
         } else {
            URL = 'Users/carregarDados/' + idFunc;
         }

         $.ajax({
            type: 'GET',
            url: URL,
            data: '',
            dataType: 'json'

         }).done(function (dados) {

            $('input').val('');
            //$('input').attr('checked', false);
            $('#ativoU').attr('checked', false);

            $.each(dados, function (campo, valor) {

               //console.log(campo+' : '+valor);

               if (campo == 'ativoU') {
                  $('#' + campo).prop('checked', valor);

               } else {
                  $('#' + campo).val(valor);
               }

            });

            $('#nomeFuncionário').html(nomeFuncionario);

            $('#modalEditarUsuario').modal('show');


         }).fail(function (jqXHR, textStatus) {
            alert('Erro: ' + textStatus + '\n' + jqXHR);

         });

      },
      Editar: function () {

         var data = $('#form-usuarios').serialize();

         $.ajax({
            type: 'POST',
            url: 'Users/Editar/',
            data: data,
            dataType: 'text'

         }).done(function (dados) {

            //return alert(dados);

            alert('Login Salvo com sucesso!!');

            //document.location = window.location.href;
            location.reload(true);

         }).fail(function (jqXHR, textStatus) {
            alert('Erro: ' + textStatus + '\n' + jqXHR);

         });
      }

   }
}