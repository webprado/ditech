
var Sistema = {
   Agendas: {
      Salvar: function (dataInicio, horaInicio, dataFim, horaFim) {

         var dados = '&datainicio=' + dataInicio + '&horainicio=' + horaInicio + '&datafim=' + dataFim + '&horafim=' + horaFim;
         dados += '&funcionarios_id=' + $('#funcionarios_id').val();
         dados += '&salas_id=' + $('#salas_id').val();
         dados += '&titulo=' + $('#title').val();
         dados += '&observacao=' + $('#body').val();

         //return alert(dados);

         $.ajax({
            type: 'POST',
            url: baseURL + 'Agendas/Salvar/',
            data: dados,
            dataType: 'text',
            success: function (data) {
               var tmp = data.split("|");
               if (tmp[0] != '')
                  var status = tmp[0];

               //return alert(data);
               alert('Agenda Salva!');

               //document.location = window.location.href;
               location.reload(true);
            }

         });
      },
      Excluir: function (idAgenda, chave) {

         $.ajax({
            type: 'GET',
            url: baseURL + 'Agendas/Excluir/' + idAgenda,
            dataType: 'text',
            success: function (dados) {

               alert('Agenda excluída!');
               //document.location = window.location.href;
               location.reload(true);
            }
         });
      }
   }
}