
var Sistemas = {
   Usuarios: {
      CarregarModalEditarUsuario: function (idFunc, idUser, nomeFuncionario) {
         
         $.ajax({
            type: 'GET',
            url: 'Usuarios/carregarDados/' + idFunc + '/' + idUser,
            data: '',
            dataType: 'json'

         }).done(function (dados) {

            $('input').val('');
            $('input').attr('checked', false);

            $.each(dados, function (campo, valor) {

               alert(campo + ' : ' + valor);

               if (campo == 'ativoU') {
                  $('#' + campo).attr('checked', valor);

               } else {
                  $('#' + campo).val(valor);
               }

            });

            $('#nomeFuncionario').html(nomeFuncionario);

            $('#modalEditarUsuario').modal('show');


         }).fail(function (jqXHR, textStatus) {
            alert('Erro: ' + textStatus + '\n' + jqXHR);

         });

      },
      Editar: function () {

         var data = $('#form-usuarios').serialize();

         $.ajax({
            type: 'POST',
            url: 'Usuarios/Editar/',
            data: data,
            dataType: 'text'

         }).done(function (dados) {

            //return alert(dados);

            alert('Login Salvo com sucesso!!');

            document.location = window.location.href;

         }).fail(function (jqXHR, textStatus) {
            alert('Erro: ' + textStatus + '\n' + jqXHR);

         });
      }

   }
}