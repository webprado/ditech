<style>
    .textCenter { text-align: center; }
    #tipoModal { font-weight: normal; }
</style>

<h2><?php echo $header; ?></h2>
<hr/>

<div style="margin-bottom: 5px;">
    <button type="button" class="btn btn-default" onclick="Sistema.Funcionarios.CarregarModalNovo()" > <i class="fa fa-floppy-o"></i> Adicionar </button>
</div>


<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>                
                <th>Nome</th>
                <th>Celular</th>                
                <th>Email</th>
                <th class='textCenter' style="width: 1%;">Ativo</th>
                <th class='textCenter'>Ações</th>
            </tr>
        </thead>
        <tbody>

            <?php
            if (count($funcionarios) > 0) {

                foreach ($funcionarios as $t) {
                    echo '<tr id="tr_' . $t['Funcionario']['id'] . '">                
                            <td>' . $t['Funcionario']['nome'] . '</td>
                            <td>' . ($t['Funcionario']['celular'] ? $t['Funcionario']['celular'] : '---') . '</td>
                            <td>' . ($t['Funcionario']['email'] ? $t['Funcionario']['email'] : '---') . '</td>
                            <td class="textCenter">
                                <input type="checkbox" id="' . $t['Funcionario']['id'] . '" onclick="Sistema.Funcionarios.Ativar(this)" ' . ($t['Funcionario']['ativo'] == 1 ? "checked" : "") . '>
                            </td>                            
                            <td class="textCenter">
                                <button type="button" class="btn btn-default btn-sm" onclick="Sistema.Funcionarios.CarregarModalEditar('.$t['Funcionario']['id'].')";  title="Editar"><i class="fa fa-pencil"></i></button>
                                &nbsp;
                                <button type="button" class="btn btn-default btn-sm" onclick="Sistemas.Usuarios.CarregarModalEditarUsuario('.$t['Funcionario']['id'].','.($t['Usuario']['id'] ? $t['Usuario']['id'] : 0).','."'".$t['Funcionario']['nome']."'".')";  title="Login de ' . strtoupper($t['Funcionario']['nome']) . '"><i class="fa fa-user"></i></i></button>
                                &nbsp;
                                <button type="button" class="btn btn-default btn-sm" onclick="Sistema.Funcionarios.Excluir(' . $t['Funcionario']['id'] . ')" title="Excluir"><i class="fa fa-times"></i></button>
                            </td>
                        </tr>';
                }
            } else {
                echo '<tr><td colspan="6" class="textCenter" >Não foram enontrados registros.</td></td>';
            }
            ?>

        </tbody>
    </table>
</div>


<div id="modalEditar" tabindex="-1" role="dialog" aria-hidden="true" aria-labbeledby="MyModal1Label" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ">
                <!-- With 'close' => true, or without specifying:
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> -->
                <h3 class="modal-title" id="MyModal1Label">Funcionários</h3>
                <label id="tipoModal">Edição</label>
            </div>
            <div class="modal-body ">    
                <form id="form-funcionarios" method="post" accept-charset="utf-8" class="form-horizontal" role="form" action="#">
                    <input type="hidden" name="id" id="id" class="form-control" />

                    <div class="form-group text">
                        <label class="col-sm-4 col-md-2 control-label"  for="sala">Nome</label>
                        <div class="col-sm-4 col-md-6">
                            <input type="text" name="nome" id="nome" class="form-control" maxlength="100"/>
                        </div>
                    </div>
                  
                    <div class="form-group text">
                        <label class="col-sm-10 col-md-2 control-label"  for="sala">Fone Fixo</label>
                        <div class="col-sm-4 col-md-4">
                            <input type="text" name="telefonefixo" id="telefonefixo" class="form-control" maxlength="15" />
                        </div>
                    </div>
                    <div class="form-group text">
                        <label class="col-sm-4 col-md-2 control-label"  for="sala">Celular</label>
                        <div class="col-sm-4 col-md-4">
                            <input type="text" name="celular1" id="celular1" class="form-control" maxlength="15" />
                        </div>
                    </div>                    
                    <div class="form-group text">
                        <label class="col-sm-4 col-md-2 control-label"  for="sala">Email</label>
                        <div class="col-sm-4 col-md-6">
                            <input type="text" name="email" id="email" class="form-control" maxlength="100" />
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer ">
                <button class="btn btn-primary btn-primary" type="submit" onclick="Sistema.Funcionarios.Editar()">Salvar</button>
                <button data-dismiss="modal" class="btn btn-default" type="submit">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalEditarUsuario" tabindex="-1" role="dialog" aria-hidden="true" aria-labbeledby="MyModal1Label" class="modal fade">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ">
                <h3 class="modal-title" id="MyModal1Label">Login</h3>
                <label id="nomeFuncionario"></label>
            </div>
            <div class="modal-body ">    
                <form id="form-usuarios" method="post" accept-charset="utf-8" class="form-horizontal" role="form" action="#">

                    <input type="hidden" name="idU" id="idU" class="form-control" />
                    <input type="hidden" name="funcionarios_id" id="funcionarios_id" class="form-control" />

                    <div class="form-group text">
                        <label class="col-sm-4 col-md-2 control-label"  for="login">Login</label>
                        <div class="col-sm-4 col-md-6">
                            <input type="text" name="login" id="login" class="form-control" maxlength="10"/>
                        </div>
                    </div>
                    <div class="form-group text">
                        <label class="col-sm-4 col-md-2 control-label"  for="senha">Senha</label>
                        <div class="col-sm-4 col-md-3">
                            <input type="password" name="senha" id="senha" class="form-control" maxlength="10" />
                        </div>
                    </div>
                  
                    <div class="form-group checkbox">
                        <label class="col-sm-4 col-md-2 control-label"  for="sala"></label>
                        <input type="checkbox" name="ativoU" id="ativoU"  /> Ativo
                    </div>

                </form>
            </div>
            <div class="modal-footer ">
                <button class="btn btn-primary btn-primary" type="submit" onclick="Sistemas.Usuarios.Editar()">Salvar</button>
                <button data-dismiss="modal" class="btn btn-default" type="submit">Fechar</button>
            </div>
        </div>
    </div>
</div>