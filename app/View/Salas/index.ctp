<style>
    .textCenter { text-align: center; }
    #tipoModal { font-weight: normal; }
</style>

<h2><?php echo $header; ?></h2>
<hr/>

<?php //echo $this->Session->flash(); ?>

<div style="margin-bottom: 5px;">
    <button type="button" class="btn btn-default" onclick="Sistema.Salas.CarregarModalNovo()" > <i class="fa fa-floppy-o"></i> Adicionar </button>
</div>


<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>                
                <th>Sala</th>
                <th class='textCenter'>Número</th>
                <th class='textCenter'>Ativo</th>
                <th class='textCenter'>Ações</th>
            </tr>
        </thead>
        <tbody>

            <?php
            
            if(count($salas)>0){
            
                foreach ($salas as $s) {
                    echo '<tr id="tr_'.$s['Sala']['id'].'">                
                            <td>' . $s['Sala']['nome'] . '</td>
                            <td class="textCenter">' . ($s['Sala']['numero'] ? $s['Sala']['numero'] : "---") . '</td>
                            <td class="textCenter">
                                <input type="checkbox" id="'.$s['Sala']['id'].'" onclick="Sistema.Salas.Ativar(this)" '.($s['Sala']['ativo']==1 ? "checked" : "").'>
                            </td>
                            <td class="textCenter">
                                <button type="button" class="btn btn-default btn-sm" onclick="Sistema.Salas.CarregarModalEditar(' . $s['Sala']['id'] . ')";  title="Editar"><i class="fa fa-pencil"></i></button>
                                &nbsp;
                                <button type="button" class="btn btn-default btn-sm" onclick="Sistema.Salas.Excluir('.$s['Sala']['id'].')" title="Excluir"><i class="fa fa-times"></i></button>
                            </td>
                        </tr>';
                }
            } else {
                echo '<tr><td colspan="4" class="textCenter" >Não foram enontrados registros.</td></td>';                
            }
            ?>

        </tbody>
    </table>
</div>


<div id="modalEditar" tabindex="-1" role="dialog" aria-hidden="true" aria-labbeledby="MyModal1Label" class="modal fade">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ">
                <!-- With 'close' => true, or without specifying:
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> -->
                <h3 class="modal-title" id="MyModal1Label">Salas</h3>
                <label id="tipoModal">Edição</label>
            </div>
            <div class="modal-body ">    
                <form id="form-salas" method="post" accept-charset="utf-8" class="form-horizontal" role="form" action="#">
                    <input type="hidden" name="id" id="id" class="form-control" />

                    <div class="form-group text">
                        <label class="col-sm-4 col-md-2 control-label"  for="sala">Sala</label>
                        <div class="col-sm-4 col-md-6">
                            <input type="text" name="nome" id="nome" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group password">
                        <label class="col-sm-4 col-md-2 control-label"  for="numero">Número</label>
                        <div class="col-sm-4 col-md-6">
                            <input type="text" name="numero" id="numero" class="form-control" />
                        </div>
                    </div>                   
                </form>
            </div>
            <div class="modal-footer ">
                <button class="btn btn-primary btn-primary" type="submit" onclick="Sistema.Salas.Editar()">Salvar</button>
                <button data-dismiss="modal" class="btn btn-default" type="submit">Fechar</button>
            </div>
        </div>
    </div>

</div>