<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <?php echo $this->Html->charset(); ?>

        <!-- Não ficar disponível para os robôs de busca -->
        <meta name="robots" content="noindex, nofollow">
        <meta name="googlebot" content="noindex, nofollow">
        <meta name="slurp" content="noindex, nofollow">
        <meta name="msnbot" content="noindex, nofollow">
        <meta name="teoma" content="noindex, nofollow">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="">
        <meta name="author" content="">

        <title>
            Ditech | Sala de Reuniões - Agenda
        </title>
        <?php
        echo $this->Html->meta('icon');

        //echo $this->Html->css('cake.generic');

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        echo $this->Html->css('bootstrap.min.css');
        echo $this->Html->css('custom.css');
        ?>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

        <link href='<?= $this->base ?>/js/datepicker/css/datepicker.css' rel='stylesheet' />

        <style>
            #logoTopo { 
                margin-top: -13px; 
                width: 90px; 
            }
        </style>

        <?php
        echo $this->Html->script($JQuery);
        echo $this->Html->script('bootstrap.min.js');
        ?>

    </head>
    <body>

        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <?php
                        echo $this->Html->image('logo.png', array('id' => 'logoTopo'));
                        ?>
                    </a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">                        
                        <li class="<?php //echo $classActiveAgenda ?>">
                            <?php
                            echo $this->Html->link(
                                    'Agenda', array(
                                'controller' => 'Agendas',
                                'action' => '')
                            );
                            ?>
                        </li>

                            <li class="dropdown <?php //echo $classActiveCadastro ?>">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Cadastros <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <?php
                                        echo $this->Html->link(
                                                'Funcionários', array(
                                            'controller' => 'Funcionarios',
                                            'action' => '')
                                        );
                                        ?>
                                    </li>

                                    <li>
                                        <?php
                                        echo $this->Html->link(
                                                'Salas', array(
                                            'controller' => 'Salas',
                                            'action' => '')
                                        );
                                        ?>
                                    <li>
                                </ul>
                            </li>

                        
                        <!--
                        <li>
                            <?php
                            echo $this->Html->link(
                                    'Sair', array(
                                'controller' => 'users',
                                'action' => 'logout')
                            );
                            ?>
                        </li>
                        -->
                    </ul>

                    <form class="navbar-form navbar-right">
                        <div class="form-group" style="font-weight: bold; font-size: 15px; color:#323232; margin-top: 10px;">
                            Olá, <?php echo $this->Session->read('User.Nome');?> | 
                            <?php
                            echo $this->Html->link(
                                                    'SAIR',
                                                    '/users/logout',
                                                    array('style'=>'color:#323232;')
                                                                                        
                            );
                            ?>
                        </div>
                    </form>

                </div><!--/.nav-collapse -->

            </div>
        </nav>

        <div class="container">
            <?php echo $this->fetch('content'); ?>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="text-muted">Rodrigo Prado &copy</p>
            </div>
        </footer>


        <?php
        echo $this->Html->script($scriptJS);
        echo $this->Html->script($scriptJS_extra);
        ?>

        <script src='<?= $this->base ?>/js/datepicker/js/bootstrap-datepicker.js'></script>


        <script>
            $('#dataIni').datepicker();
            $('#dataFim').datepicker();
            $('#dpMonths').datepicker({
                language: "pt-BR"
            });
        </script>

        <style>
            .input-append.date .add-on i, .input-prepend.date .add-on i{
                display: inline-block;
            }
        </style>

    </body>
</html>