<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php
        echo $this->Html->charset();
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        echo $this->Html->css('bootstrap.min.css');
        //echo $this->Html->css('custom.css');
        ?>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

        <link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap.css" rel="stylesheet">

        <?php
        //echo $this->Html->script('jquery.min.js');
        //echo $this->Html->script('bootstrap.min.js');
        ?>

        <style>

            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #f5f5f5;
            }

            .form-signin {
                max-width: 300px;
                padding: 19px 29px 29px;
                margin: 0 auto 20px;
                background-color: #fff;
                border: 1px solid #e5e5e5;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            .form-signin input[type="text"],
            .form-signin input[type="password"] {
                font-size: 16px;
                height: auto;
                margin-bottom: 15px;
                padding: 7px 9px;
            }

            h2 { font-size: 20px; }
        </style>

        <link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap-responsive.css" rel="stylesheet">

    </head>
    <body >

        <div class="container">
            <!--<form class="form-signin">-->
                <?php echo $this->Form->create(null,array('class'=>'form-signin','url'=>array('controller' => 'users', 'action' => 'login'))); ?>
                <div style="text-align:center;">
                    <?php
                    echo $this->Html->image('logo.png', array('id' => 'logoTopo'));
                    ?>
                    <h2 class="form-signin-heading">Sala de Reuniões<br/>Agenda</h2>
                </div>

                <input type="text" class="input-block-level" placeholder="Login" name="login" id="login">
                <input type="password" class="input-block-level" placeholder="Senha" name="senha" id="senha">

                <?php echo $this->Session->flash(); ?>
                
                <div style="text-align:center;">
                    <button class="btn btn-default btn-primary" type="submit"><i class="fa fa-check-circle-o"></i>&nbsp;Entrar</button>
                </div>
            </form>
        </div>

        
    </body>
</html>