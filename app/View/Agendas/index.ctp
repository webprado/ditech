<script>
    var baseDIR = '<?php echo $this->base; ?>';
    var baseURL = '<?php echo Router::url('/', true); ?>';
    var jsonInfo = <?php echo $jsonInfo ?>;
</script>

<link href='<?= $this->base ?>/js/jquery-weekcalendar-1.2.2/full_demo/jquery-ui.css' rel='stylesheet' />
<link href='<?= $this->base ?>/js/jquery-weekcalendar-1.2.2/jquery.weekcalendar.css' rel='stylesheet' />
<link href='<?= $this->base ?>/js/jquery-weekcalendar-1.2.2/full_demo/demo.css' rel='stylesheet' />

<script src='<?= $this->base ?>/js/jquery-weekcalendar-1.2.2/full_demo/jquery.min.1.3.2.js'></script>
<script src='<?= $this->base ?>/js/jquery-weekcalendar-1.2.2/full_demo/jquery-ui.min.1.7.2.js'></script>
<script src='<?= $this->base ?>/js/jquery-weekcalendar-1.2.2/jquery.weekcalendar.js'></script>
<script src='<?= $this->base ?>/js/agendas.js'></script>
<script src='<?= $this->base ?>/js/jquery-weekcalendar-1.2.2/full_demo/demo.js'></script>


<h2><?php echo $header; ?></h2>

<div style="text-align: center">
   <?php
   $btnPrimary = '';

   foreach ($salas as $s) {

       if ($idSala == $s['salas']['id']) {
           $btnPrimary = 'btn-primary';
       } else {
           $btnPrimary = '';
       }
       echo '<a href="' . $this->base . '/Agendas/Salas/' . $s['salas']['id'] . '" class="btn btn-default ' . $btnPrimary . '" ><span class="glyphicon glyphicon-calendar"></span>&nbsp;' . $s['salas']['nome'] . '</a>&nbsp;';
   }
   ?>
</div>
<hr/>

<div id='calendar'></div>
<br/>

<input type="hidden" value="<?php echo $this->Session->read('User.funcionarioID'); ?>" name="funcionarios_id" id="funcionarios_id"/>
<input type="hidden" value="<?php echo $idSala; ?>" name="salas_id" id="salas_id"/>

<div id="event_edit_container" >
   <form id="form-agenda" name="form-agenda">        

      <input type="hidden" value="" name="id" id="id"/>
      <input type="hidden" value="" name="chave" id="chave"/>

      <ul>
         <li>
            <span>Data: </span><span class="date_holder"></span> 
         </li>
         <li>
            <label for="start">Início: </label><select name="start" id="start"><option value="">Select Start Time</option></select>
         </li>
         <li>
            <label for="end">Término: </label><select name="end" id="end"><option value="">Select End Time</option></select>
         </li>
         <li>
            <label for="title">Descrição: </label><input type="text" name="title" id="title" />
         </li>

         <li class="body">
            <label for="body">Observação: </label><textarea name="body" id="body"></textarea>
         </li>

        
      </ul>
   </form>
</div>

<script>
    var idFuncionario = '<?php echo $this->Session->read('User.funcionarioID'); ?>';
</script>