<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController {

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);

        $this->set('JQuery', 'jquery.min.js');
    }

    public function Login($expirado=null) {

        if ($this->request->is('post')) {

            $login = strtolower(trim($this->request->data['login']));
            $senha = strtolower(trim($this->request->data['senha']));

            $sql = "SELECT * 
                        FROM users u 
                    LEFT JOIN funcionarios t ON t.id = u.funcionarios_id
                    WHERE u.login='" . $login . "' 
                    AND u.senha='" . $senha . "'
                    AND u.ativo=1
                    AND u.excluido=0";

            $res = $this->User->query($sql);
           
            if ($res) {
                $this->Session->write('User.Logado', 'S');
                $this->Session->write('User.Id', $res[0]['u']['id']);
                $this->Session->write('User.Nome', $res[0]['t']['nome']);
                $this->Session->write('User.funcionarioID', $res[0]['u']['funcionarios_id']);

                $this->redirect(array('controller' => 'Agendas', 'action' => 'Index'));
                
            } else{                
                $this->Session->destroy();
                $this->layout = 'login';
                $this->Session->setFlash(__('<div class="alert alert-danger alert-dismissible text-center" role="alert">  
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> 
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                Usuário ou senha inválidos!
                                            </div>'));
            }
            
        } else if($expirado){
            $this->layout = 'login';
            $this->Session->setFlash(__('<div class="alert alert-danger alert-dismissible text-center" role="alert">  
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> 
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                Sessão expirou!<br>
                                                Acesse novamente.
                                            </div>'));
            
        } else {
            $this->Session->destroy();
            $this->layout = 'login';
        }
    }

    public function Logout() {        
        $this->Session->destroy();
        $this->redirect('login');
    }

    public function carregarDados($funcionarios_id, $id = null) {

        $arrDados = array();
        $arrDados['funcionarios_id'] = $funcionarios_id;

        $this->User->id = $id == null || $id == 0 ? null : $id;

        if ($this->User->exists()) {

            $this->request->data = $this->User->read(null, $id);

            foreach ($this->request->data as $u) {
                $arrDados['idU'] = $u['id'];
                $arrDados['login'] = $u['login'];
                $arrDados['senha'] = $u['senha'];
                $arrDados['ativoU'] = $u['ativo'] == 1 ? true : false;
            }
        }

        $jsonDados = json_encode($arrDados);

        echo $jsonDados;
        die;
    }

    public function Editar() {

        $this->User->id = $this->request->data['id'] ? $this->request->data['id'] : null;

        if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->request->data['idU']) {
                $this->request->data['id'] = $this->request->data['idU'];
            }

            $this->request->data['ativo'] = isset($this->request->data['ativoU']) ? 1 : 0;

            //print_r($this->request->data);
            //die;

            if ($this->User->save($this->request->data)) {
                echo "Salvo";
                die;
            } else {
                return false;
            }
        }
    }

}
