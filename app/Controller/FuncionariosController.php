<?php

App::uses('AppController', 'Controller');

class FuncionariosController extends AppController {

    public $uses = array();

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);

        $this->set('JQuery', 'jquery.min.js');
        $this->set('scriptJS', 'funcionarios.js');
        $this->set('scriptJS_extra', 'users.js');
        $this->set('header', 'Funcionários');
        $this->set('classActiveCadastro', 'active');
    }

    public function index() {        
         if (!$this->Session->check('User.Logado')) {
            $this->Session->setFlash('Your stuff has been saved.');
            $this->redirect(array('controller' => 'Users', 'action' => 'Login','S'));
        }
        
        $this->set('cabecalho', 'Funcionários');

        $this->Funcionario->recursive = 0;
       
        $sql = "SELECT * FROM funcionarios Funcionario
                    LEFT JOIN users Usuario ON Usuario.funcionarios_id = Funcionario.id
                   WHERE Funcionario.excluido = 0
                ORDER BY Funcionario.nome ASC";
        $funcionarios = $this->Funcionario->query($sql);
        
        $this->set('funcionarios', $funcionarios);
    }

    public function Editar() {

        $this->Funcionario->id = $this->request->data['id'] ? $this->request->data['id'] : null;

        if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->Funcionario->save($this->request->data)) {
                echo "Salvo";
                die;
            } else {
                return false;
            }
        }
    }

    public function carregarDados($id = null) {
        $this->Funcionario->id = $id;

        if ($this->Funcionario->exists()) {

            $this->request->data = $this->Funcionario->read(null, $id);
           
            $arrDados = array();
            foreach ($this->request->data as $t) {
                $arrDados['id'] = $t['id'];
                $arrDados['nome'] = $t['nome'];
                $arrDados['telefonefixo'] = $t['telefonefixo'];
                $arrDados['celular'] = $t['celular'];
                $arrDados['email'] = $t['email'];
            }
            $jsonDados = json_encode($arrDados);

            echo $jsonDados;
            die;
        }
    }

    public function Ativar($id = null, $check = 1) {

        $this->Funcionario->id = $id;

        if ($this->Funcionario->exists()) {

            $dados = array();
            $dados['Funcionario']['ativo'] = $check;

            $this->Funcionario->save($dados);

            die;
        }
    }

    public function Excluir($id = null) {

        $this->Funcionario->id = $id;

        if ($this->Funcionario->exists()) {

            $dados = array();
            $dados['Funcionario']['excluido'] = 1;

            $this->Funcionario->save($dados);

            die;
        }
    }

}
