<?php

App::uses('AppController', 'Controller');

class SalasController extends AppController {

    public $uses = array();
    
    public function __construct($request = null, $response = null) {
        
        parent::__construct($request, $response);

        $this->set('JQuery', 'jquery.min.js');
        $this->set('scriptJS','salas.js?param='.date('Hms'));
        $this->set('header', 'Salas');
        $this->set('classActiveCadastro', 'active');
      
    }

    public function Index() {
        
         if (!$this->Session->check('User.Logado')) {
            $this->Session->setFlash('Your stuff has been saved.');
            $this->redirect(array('controller' => 'users', 'action' => 'login','S'));
        }
        
        $this->set('cabecalho', 'Salas');

        $this->sala->recursive = 0;
        $this->paginate = array('conditions' => array('excluido' => 0), 'order' => 'nome ASC');
        $this->set('salas', $this->paginate());
    }

    public function Editar() {
        
        $this->Sala->id = $this->request->data['id'] ? $this->request->data['id'] : null;
               
        if ($this->request->is('post') || $this->request->is('put')) {
            
            if ($this->Sala->save($this->request->data)) {
                 echo "Salvo";
                die;
                
            } else {
                return false;
            }            
        }       
    }
    
    public function carregarDados($id=null){
        $this->Sala->id = $id;
        
        if ($this->Sala->exists()) {
            
            $this->request->data = $this->Sala->read(null, $id);
                
                $arrDados = array();
                foreach ($this->request->data as $s) {
                    $arrDados['id'] = $s['id'];
                    $arrDados['nome'] = $s['nome'];
                    $arrDados['numero'] = $s['numero'];
                    $arrDados['ativo'] = $s['ativo'];
                }

                $jsonDados = json_encode($arrDados);

                echo $jsonDados;
                die;            
        }        
    }

    public function Ativar($id = null, $check = 1) {    
       
         $this->Sala->id = $id;
        
        if ($this->Sala->exists()) {            
            
            $dados = array();            
            $dados['Sala']['ativo'] = $check;
            
            $this->Sala->save($dados);
            
            die;            
        }        
    }
    
    public function Excluir($id=null){
        
        $this->Sala->id = $id;
      
        if ($this->Sala->exists()) {
            
            $dados = array();            
            $dados['Sala']['excluido'] = 1;
            $dados['Sala']['ativo'] = 0;
                       
            $this->Sala->save($dados);
            
            $this->loadModel('Agendas');
            $this->Agendas->deleteAll(array('Agendas.salas_id' => $id));
            
            die;
            
        }
        
    }

}