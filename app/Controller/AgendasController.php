<?php

App::uses('AppController', 'Controller');
App::uses('File', 'Utility');

class AgendasController extends AppController {

    //public $uses = array();

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);

        $this->set('JQuery', 'jquery.min.js');
        $this->set('scriptJS', 'agendas.js');
        $this->set('header', 'Agendas');
        $this->set('classActiveAgenda', 'active');
    }

    public function Index($idSala = null) {

        if (!$this->Session->check('User.Logado')) {
            $this->Session->setFlash('Your stuff has been saved.');
            $this->redirect(array('controller' => 'users', 'action' => 'login', 'S'));
        }

        $this->set('cabecalho', 'Agendas');

        $this->loadModel('Salas');
        $sql = "SELECT * FROM salas WHERE ativo=1 AND excluido=0";
        $salas = $this->Salas->query($sql);
        $this->set(compact('salas'));

        $primeiraSala = $this->Salas->find('first', array('conditions' => array('Salas.ativo' => 1, 'Salas.excluido' => 0))
        );

        /*
          echo '<pre>';
          print_r($primeiraSala);
          die;
         * 
         */


        $idSala = $idSala == null ? $primeiraSala['Salas']['id'] : $idSala;

        $this->set('idSala', $idSala);

        $dirJson = APP . '/webroot/agenda_json';
        $arqJson = new File($dirJson . "/dados.json");

        if ($arqJson->exists()) {

            $sql = "SELECT a.*, t.nome AS funcionario 
                    FROM agendas a 
                        INNER JOIN funcionarios t ON t.id = a.funcionarios_id
                        INNER JOIN salas s ON s.id = a.salas_id
                    WHERE a.salas_id = " . $idSala . " 
                    AND a.ativo=1 AND a.excluido=0";
            $res = $this->Agenda->query($sql);

            $dados = array();
            $i = 0;
            $json = '[';
            foreach ($res as $r) {

                list($ano, $mes, $dia) = explode('-', $r['a']['datainicio']);
                list($hora, $min, $seg) = explode(':', $r['a']['horainicio']);
                $mes = intval($mes) - 1;
                $dados[$i]['start'] = "new Date($ano, $mes, $dia, $hora, $min)";

                list($ano, $mes, $dia) = explode('-', $r['a']['datafim']);
                list($hora, $min, $seg) = explode(':', $r['a']['horafim']);
                $mes = intval($mes) - 1;
                $dados[$i]['end'] = "new Date($ano, $mes, $dia, $hora, $min)";

                list($nome) = explode(" ", $r['t']['funcionario']);

                $json .= '{';

                $json .= '"id": ' . $r['a']['id'] . ',
                            "id_funcionario": ' . $r['a']['funcionarios_id'] . ',
                            "title": "' . strtoupper($nome) . ' | ' . $r['a']['titulo'] . '",
                            "start": ' . $dados[$i]['start'] . ',
                            "end": ' . $dados[$i]['end'] . ',
                            "chave": "' . $r['a']['chave'] . '",
                            "body": "' . $r['a']['observacao'] . '"';

                $json .= '},' . "\n";

                $i++;
            }

            $json .= ']';

            $this->set('jsonInfo', $json);
        }

        // ===============================================================================================================================================================
        // ===============================================================================================================================================================
        $dataIni = date('Y-m-d', mktime(0, 0, 0, 5, 29, 2015));
        list($ano, $mes, $dia) = explode('-', $dataIni);

        $diaSemanaX = date('D', mktime(0, 0, 0, $mes, $dia, $ano));

        $anofinal = $ano + 1;

        while (intval($ano) < intval($anofinal)) {

            $num = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
            $dataIni = date('Y-m-d', strtotime("+$num days", strtotime($dataIni)));
            list($ano, $mes, $dia) = explode('-', $dataIni);

            //echo $diaSemanaX.' >>> '.date('D', mktime(0, 0, 0, $mes, $dia, $ano)).' >>>>>>>>> '.$dataIni.'<br>';

            while (date('D', mktime(0, 0, 0, $mes, $dia, $ano)) == "Sat" ||
            date('D', mktime(0, 0, 0, $mes, $dia, $ano)) == "Sun" ||
            date('D', mktime(0, 0, 0, $mes, $dia, $ano)) != $diaSemanaX) {

                $dataIni = date('Y-m-d', strtotime("-1 days", strtotime($dataIni)));
                list($ano, $mes, $dia) = explode('-', $dataIni);
                $diaSemana = date('D', mktime(0, 0, 0, $mes, $dia, $ano));
            }

            //echo $dataIni . '===' . date('D', mktime(0, 0, 0, $mes, $dia, $ano)) . ' !!!!<br><br><br>';

            $dataIni = date('Y-m-d', mktime(0, 0, 0, $mes, 29, $ano));
        }
        //die;
        // ===============================================================================================================================================================
        // ===============================================================================================================================================================


        $this->render('index');
    }

    public function Salas($idSala = null) {

        $this->Index($idSala);

        $this->render('index');
    }

    public function Salvar() {

        $this->request->data['chave'] = md5(date('dmYHis'));

        $this->SalvarEvento();
    }

    public function SalvarEvento() {

        $this->request->data['datainicio'] = $this->ajustaData($this->request->data['datainicio']);
        $this->request->data['datafim'] = $this->ajustaData($this->request->data['datafim']);
        $this->request->data['horainicio'] = $this->ajustaHora($this->request->data['horainicio']);
        $this->request->data['horafim'] = $this->ajustaHora($this->request->data['horafim']);

        list($ano, $mes, $dia) = explode('-', $this->request->data['datainicio']);
        $this->request->data['diasemana'] = date('D', mktime(0, 0, 0, $mes, $dia, $ano));

        $this->Agenda->create();
        $this->Agenda->save($this->request->data);

        die;
    }

    public function Excluir($idSala = null) {

        $this->Agenda->id = $idSala;

        if ($this->Agenda->exists()) {

            $dados = array();
            $dados['Agenda']['ativo'] = 0;
            $dados['Agenda']['excluido'] = 1;

            $this->Agenda->save($dados);

            die;
        }
    }

    public function ajustaData($data) {

        list($ano, $mes, $dia) = explode('-', $data);
        $mes = str_pad($mes, 2, '0', STR_PAD_LEFT);
        $dia = str_pad($dia, 2, '0', STR_PAD_LEFT);

        return $ano . '-' . $mes . '-' . $dia;
    }

    public function ajustaHora($hora) {
        list($hora, $min, $seg) = explode(':', $hora);
        $hora = str_pad($hora, 2, '0', STR_PAD_LEFT);
        $min = str_pad($min, 2, '0', STR_PAD_LEFT);
        $seg = str_pad($seg, 2, '0', STR_PAD_LEFT);

        return $hora . ':' . $min . ':' . $seg;
    }

}
