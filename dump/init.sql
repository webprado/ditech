-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table ditech.agendas
CREATE TABLE IF NOT EXISTS `agendas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `funcionarios_id` int(11) NOT NULL,
  `salas_id` int(11) NOT NULL,
  `horainicio` time DEFAULT NULL,
  `datainicio` date DEFAULT NULL,
  `horafim` time DEFAULT NULL,
  `datafim` date DEFAULT NULL,
  `diasemana` varchar(10) DEFAULT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `observacao` text,
  `ativo` int(11) NOT NULL DEFAULT '1',
  `excluido` int(11) NOT NULL DEFAULT '0',
  `chave` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`,`funcionarios_id`,`salas_id`),
  KEY `fk_agendas_profissionals1` (`funcionarios_id`),
  KEY `fk_agendas_salas1` (`salas_id`),
  CONSTRAINT `fk_agendas_funcionarios1` FOREIGN KEY (`funcionarios_id`) REFERENCES `funcionarios` (`id`),
  CONSTRAINT `fk_agendas_salas1` FOREIGN KEY (`salas_id`) REFERENCES `salas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table ditech.agendas: ~5 rows (approximately)
/*!40000 ALTER TABLE `agendas` DISABLE KEYS */;
INSERT INTO `agendas` (`id`, `funcionarios_id`, `salas_id`, `horainicio`, `datainicio`, `horafim`, `datafim`, `diasemana`, `titulo`, `observacao`, `ativo`, `excluido`, `chave`) VALUES
	(1, 1, 1, '10:45:00', '2017-06-11', '11:15:00', '2017-06-11', 'Sun', 'Reserva Rodrigo', '', 0, 1, 'dfd4be17f94ddfadc5ebefb6ed61ce3a'),
	(2, 2, 1, '11:45:00', '2017-06-11', '12:15:00', '2017-06-11', 'Sun', 'Reserva Andrea', '', 1, 0, 'd17d67fceac5fd677a545155a382b0b6'),
	(3, 1, 2, '11:45:00', '2017-06-11', '12:15:00', '2017-06-11', 'Sun', 'Teste Rodrigo', 'Aqui Ã© uma obervaÃ§Ã£o', 1, 0, '7450870e7913fabddade706f520f6bc4'),
	(4, 1, 1, '12:15:00', '2017-06-13', '12:45:00', '2017-06-13', 'Tue', 'Entrevista Fulano', 'Canditado para PHP', 1, 0, '839ad53643972a82bfe3abb00fe1f44a'),
	(5, 2, 1, '10:45:00', '2017-06-14', '12:00:00', '2017-06-14', 'Wed', 'Vendas - Maio 2017', 'Resultados dos negÃ³cios fechados em maio/2017', 1, 0, 'a8a6174331474a583d410a6b7c6d801e');
/*!40000 ALTER TABLE `agendas` ENABLE KEYS */;


-- Dumping structure for table ditech.funcionarios
CREATE TABLE IF NOT EXISTS `funcionarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `telefonefixo` varchar(15) DEFAULT NULL,
  `celular` varchar(15) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `ativo` int(11) DEFAULT '1',
  `excluido` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table ditech.funcionarios: ~1 rows (approximately)
/*!40000 ALTER TABLE `funcionarios` DISABLE KEYS */;
INSERT INTO `funcionarios` (`id`, `nome`, `telefonefixo`, `celular`, `email`, `ativo`, `excluido`) VALUES
	(1, 'Rodrigo Prado', '3333-4444', '9999-8888', 'rodrigo@dominio.com', 1, 0),
	(2, 'Andrea Reis', '3333-4444', NULL, 'rodrigo@dominio.com', 1, 0);
/*!40000 ALTER TABLE `funcionarios` ENABLE KEYS */;


-- Dumping structure for table ditech.salas
CREATE TABLE IF NOT EXISTS `salas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `numero` varchar(10) DEFAULT NULL,
  `ativo` int(11) DEFAULT '1',
  `excluido` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table ditech.salas: ~2 rows (approximately)
/*!40000 ALTER TABLE `salas` DISABLE KEYS */;
INSERT INTO `salas` (`id`, `nome`, `numero`, `ativo`, `excluido`) VALUES
	(1, 'Sala 01', '01', 1, 0),
	(2, 'Sala Pequena', '102', 1, 0),
	(3, 'Sala Grande', '', 1, 0);
/*!40000 ALTER TABLE `salas` ENABLE KEYS */;


-- Dumping structure for table ditech.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `funcionarios_id` int(11) NOT NULL,
  `login` varchar(10) DEFAULT NULL,
  `senha` varchar(10) DEFAULT NULL,
  `ativo` int(11) NOT NULL DEFAULT '1',
  `excluido` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`funcionarios_id`),
  KEY `fk_users_profissionals1` (`funcionarios_id`),
  CONSTRAINT `fk_users_funcionarios1` FOREIGN KEY (`funcionarios_id`) REFERENCES `funcionarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table ditech.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `funcionarios_id`, `login`, `senha`, `ativo`, `excluido`) VALUES
	(1, 1, 'prado', '102030', 1, 0),
	(2, 2, 'andrea', '102030', 1, 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
