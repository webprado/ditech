
SELECT 
	d.dept_name AS Departamento,
	CONCAT(e.first_name," ",e.last_name) AS Funcionario,
	DATEDIFF(to_date,from_date) AS TempoTrabalho
FROM dept_emp de
	INNER JOIN employees e ON e.emp_no = de.emp_no
	INNER JOIN departments d ON d.dept_no = de.dept_no
WHERE from_date IS NOT NULL 
AND to_date IS NOT NULL
ORDER BY DATEDIFF(to_date,from_date) DESC
LIMIT 10;